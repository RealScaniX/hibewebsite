// webpack.config.js

const HtmlPlugin = require('html-webpack-plugin')
const webpack = require('webpack');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

//var package = require('package.json');

module.exports = {
  // Tell webpack to start bundling our app at app/index.js
  entry: {
    index: './app/index.js',
//    vendor: Object.keys(package.dependencies),
    // media: "./app/media.js"
  },

  // Output our app to the dist/ directory
  output: {
    filename: 'js/[name].app.js',
    path: __dirname + '/../www'
  },

  resolve: {
    modules: [
      'app/img',
      'node_modules',
      'static'
    ]
  },

  // Emit source maps so we can debug our code in the browser
  devtool: 'source-map',

  // Tell webpack to run our source code through Babel
  module: {

    loaders: [
        {
            test: /\.js$/,
            exclude: /node_modules/,
            loader: 'babel-loader',
            query: {
                presets: ["es2015", "react"]
            }
          },
          {
            test: /\.txt$/,
            exclude: /node_modules/,
            loader: 'raw-loader',
        },
            //   {
    //     test: /\.(jp?eg|png|gif|css)$/,
    //     exclude: /node_modules/,
    //     loader: 'file-loader'
    //   }
    ]
  },

  // Since Webpack only understands JavaScript, we need to
  // add a plugin to tell it how to handle html files.
  plugins: [
    new CopyWebpackPlugin([{
        from: __dirname + '/static/**',
        to: __dirname + '/../www',
        force: true,
        cache: false,
    }], {debug: 'debug', copyUnmodified: true, context: "static"}),

    new HtmlPlugin({
        template: 'app/templates/index.html',
        chunks: ['index'],
        filename: 'index.html'
    }),
    /*
    new webpack.optimize.CommonsChunkPlugin({
        name: "commons",
        filename: "js/commons.js",
    }),
    new UglifyJSPlugin({
        sourceMap: true
    }),
    new webpack.DefinePlugin({
        'process.env.NODE_ENV': JSON.stringify('production')
    }),
    */
  ]
}