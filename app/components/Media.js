import React from 'react'
import ReactDOM from 'react-dom'

// My stuff
import HiBePage from './HiBePage'
import * as common from '../js/common';

class VideoBox extends React.Component {
    constructor(props) {
        super(props);

    }
    render() {
        return <div className="videobox">
            <h1>{this.props.title}</h1>
            <iframe className="videobox" width="960" height="540" src={'https://www.youtube.com/embed/' + this.props.video + '?controls=1'}>
            </iframe>
            <br/>{this.props.description}
        </div>
    }
}

export default class Media extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: null,
        };
    }
    render() {
        return (
            <div>
                <VideoBox title="Teaser A" video="ZQXx_Mr2s98" description="The first teaser of the game showing off the variety of puzzles without much actual gameplay."/>
            </div>
        );
    }
}
