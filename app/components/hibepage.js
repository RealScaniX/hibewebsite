import React from 'react'

import HiBeNav from './NavBar'

class HiBeFooterTopLink extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return <div className={'footerlink dynamicElement ' + (!this.props.selected ? 'dynamicElementInvisible' : '')}><a href='#top'>top</a></div>
    }
}

class HiBeFooter extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            scrolled: false
        };
        window.addEventListener("scroll", () => {
            this.setState({scrolled: window.scrollY > 0});
        });
    }
    render() {
        return <div className="footer"><HiBeFooterTopLink selected={this.state.scrolled}/>(C) 2016-2018 Burkhard Wick</div>
    }
}

export default class HiBePage extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return <div><HiBeNav/><div className="pagecontent">{this.props.children}</div><HiBeFooter/></div>
    }
}