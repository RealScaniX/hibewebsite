import React from 'react'
import ReactDOM from 'react-dom'

// My stuff
import HiBePage from './HiBePage'
import aboutText from '../content/about.txt';
import * as common from '../js/common';

class HiBeLogo extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return <img src={'img/HiBe_' + this.props.style + '.png'}/>;
    }
}

export default class About extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            text: aboutText
        };
    }
    render() {
        return (
            <div>
                <div style={{backgroundImage:"url(../img/HiBe_small.png)", width:"550px", height:"350px", margin: "0 auto 30pt auto"}}></div>
                {common.ReactTextNode(aboutText)}
            </div>
        );
    }
}
