import React from 'react'
import {BrowserRouter as Router, Route, Link} from 'react-router-dom'

// My stuff
import * as common from '../js/common.js';

export default class HiBeNav extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            visible: true,
            mouseY: 0,
            height: 0
        }
    }
    componentDidMount() {
        this.setState({ height: this.divElement.clientHeight });
        window.addEventListener("scroll", () => {
            this.updateState();
        });
        window.addEventListener("mousemove", (e) => {
            this.state.mouseY = e.clientY;
            this.updateState();
        });
        window.addEventListener("mouseenter", (e) => {
            this.state.mouseY = e.clientY;
            this.updateState();
        });
        this.updateState();
    }
    render() {
        return <div className="dynamic navbar" style={this.state.visible ? {top:0} : {top:"-2em"}} ref={ (divElement) => this.divElement = divElement}>
            <NavButton title="About" target="/"/>
            <NavButton title="Media" target="/media"/>
            <NavButton title="Contact" target="/contact"/>
        </div>
    }
    updateState() {
        this.setState({visible: window.scrollY <= 10 || this.state.mouseY < this.state.height});
    }
}

export class NavButton extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selected: false
        }
        this.state.selected = this.isSelected();
        document.body.addEventListener("pageChange", () => {this.setState({selected: this.isSelected()})});
    }
    render() {
        return <div><Link className={'dynamic navbutton ' + (this.state.selected ? 'selected' : '')} to={this.props.target}>{this.props.title}</Link></div>
    }
    isSelected() {
        return document.location.href.endsWith(this.props.target) || (this.props.target == "index.html" && !document.location.href.endsWith(".html"));
    }
}

