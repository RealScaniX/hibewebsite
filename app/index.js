import React from 'react'
import ReactDOM from 'react-dom'
//import {render} from 'react-dom'
import {Router, Route, Link} from 'react-router-dom'
import createBrowserHistory from 'history/createBrowserHistory'

// My stuff
import HiBeNav from './components/NavBar';
import HiBePage from './components/HiBePage';
import About from './components/About'
import Media from './components/Media'
import * as common from './js/common';

// ========================================

// Pass in explicit history to add listener to it
const history = createBrowserHistory();

ReactDOM.render(
    <Router history={history}>
        <HiBePage>
            <Route exact path="/" component={About}/>
            <Route path="/media" component={Media}/>
            <Route path="/contact" component={Media}/>
        </HiBePage>
    </Router>,
    document.getElementById('content')
);

// For page changes (updating of nav buttons)
history.listen(location => common.firePageChange());

