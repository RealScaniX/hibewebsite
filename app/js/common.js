import React from 'react'

export function emToPx(em) {
    var dummy = document.createElement('div');
    dummy.style.height = em + "em";
    //var h = parseFloat(getComputedStyle(dummy).height);
    var h = dummy.offsetHeight;
    window.console.log("Height of " + em + "em in pixels: " + h);
    return h;
}

export function ReactTextNode(text) {
    text = "<p>" + text + "</p>";
    text = text.replace(/\r/g, ""); // kill weird windows returns
    text = text.replace(/\n\n/g, "</p><p>"); // new paragraphs with 2 linefeeds
    text = text.replace(/\n/g, "<br/>"); // normal linefeed with a single linefeed
    text = text.replace(/(?:<p>)?#([^\n<]*)(?:<\/p>|<br\/>)?/g, "<h1>$1</h1>"); // headers prefixed with #
    text = text.replace(/\*([^*]*)\*/g, "<span class=\"codeword\">$1</span>"); // codeword spans with * around it
    text = text.replace(/<p><\/p>/g, "<p>&nbsp;</p>"); // add nonbreakable space to empty paragraphs
    return ReactHtmlNode(text);
}

export function ReactHtmlNode(html) {
    var divElem = <div dangerouslySetInnerHTML={{__html: html}}/>;
    return divElem;
}

export function firePageChange() {
    var pageChange = new Event("pageChange");
    document.body.dispatchEvent(pageChange);
}
